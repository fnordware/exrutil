//
//  exrutil.cpp
//  exrutil
//
//  Created by Brendan Bolles on 10/7/22.
//

#include <ImfMultiPartInputFile.h>
#include <ImfInputPart.h>
#include <ImfPartType.h>
#include <ImfChannelList.h>
#include <ImfOutputFile.h>

#include <IlmThread.h>
#include <IlmThreadPool.h>

#include <string>
#include <map>
#include <exception>
#include <iostream>

#include <mach/mach.h>
#include <unistd.h>

using namespace std;
using namespace Imf;
using namespace Imath;
using namespace Iex;
using namespace IlmThread;


class FloatToHalfTask : public Task
{
  public:
	FloatToHalfTask(TaskGroup *group, const float *input_row, half *output_row, int length);
	virtual ~FloatToHalfTask() {}
	
	virtual void execute();

  private:
	const float *_input_row;
	half *_output_row;
	const int _length;
};

FloatToHalfTask::FloatToHalfTask(TaskGroup *group, const float *input_row, half *output_row, int length) :
	Task(group),
	_input_row(input_row),
	_output_row(output_row),
	_length(length)
{

}

void FloatToHalfTask::execute()
{
	const float *input_row = _input_row;
	half *output_row = _output_row;

	for(int i=0; i < _length; i++)
	{
		*output_row++ = *input_row++;
	}
}


static bool FileExists(const char *path)
{
	const int err = access(path, F_OK);
	
	return (err == 0);
}

int main(int argc, const char * argv[])
{
	try
	{
		bool argError = false;
	
		string inputPath;
		string discardPath;
		int startFrame = INT_MIN;
		int endFrame = INT_MAX;
		bool convertToHalf = false;
		
		for(int i=1; i < argc && !argError; i++)
		{
			if(inputPath.empty())
			{
				inputPath = argv[i];
				
				if(inputPath.find("%04d") == string::npos)
				{
					cout << "Input path must have %04d" << endl;
					return 1;
				}
			}
			else if(discardPath.empty())
			{
				discardPath = argv[i];
				
				if(discardPath.find("%04d") == string::npos)
				{
					cout << "Discard path must have %04d" << endl;
					return 1;
				}
			}
			else if(startFrame == INT_MIN)
			{
				try
				{
					startFrame = stoi(argv[i]);
				}
				catch(...)
				{
					cout << "Error getting start frame: not a number" << endl;
					return 1;
				}
			}
			else if(endFrame == INT_MAX)
			{
				try
				{
					endFrame = stoi(argv[i]);
				}
				catch(...)
				{
					cout << "Error getting end frame: not a number" << endl;
					return 1;
				}
			}
			else if(argv[i] == string("--convertToHalf"))
			{
				convertToHalf = true;
			}
			else
				argError = true;
		}
		
		if(argError)
		{
			cout << "Error parsing arguments." << endl;
			return 1;
		}
		
		if(inputPath.empty())
		{
			cout << "No input path provided." << endl;
			return 1;
		}
	
		if(discardPath.empty())
		{
			cout << "No discard path provided." << endl;
			return 1;
		}
		
		if(startFrame == INT_MIN)
		{
			cout << "No start frame provided." << endl;
			return 1;
		}

		if(endFrame == INT_MIN)
		{
			cout << "No end frame provided." << endl;
			return 1;
		}
		
		const string::size_type lastSlash = inputPath.find_last_of("/");
		if(lastSlash == string::npos)
		{
			cout << "Bad input path." << endl;
			return 1;
		}
		
		const string tempPath = inputPath.substr(0, lastSlash + 1) + "temp_" + inputPath.substr(lastSlash + 1);
		
		
		for(int f=startFrame; f <= endFrame; f++)
		{
		#define MAX_PATH_LEN 512
		
			char inputFramePath[MAX_PATH_LEN];
			const int inputFrameErr = snprintf(inputFramePath, MAX_PATH_LEN, inputPath.c_str(), f);
			
			if(inputFrameErr < 0)
			{
				cout << "Error parsing input path." << endl;
				return 1;
			}
			
			if(!FileExists(inputFramePath))
			{
				cout << "Missing input file " << inputFramePath << endl;
				return 1;
			}
			
			char tempFramePath[MAX_PATH_LEN];
			const int tempFrameErr = snprintf(tempFramePath, MAX_PATH_LEN, tempPath.c_str(), f);
			
			if(tempFrameErr < 0)
			{
				cout << "Error parsing temp path." << endl;
				return 1;
			}
			
			if(FileExists(tempFramePath))
			{
				cout << "Temp location already full " << tempFramePath << endl;
				return 1;
			}
			
			char discardFramePath[MAX_PATH_LEN];
			const int discardFrameErr = snprintf(discardFramePath, MAX_PATH_LEN, discardPath.c_str(), f);
			
			if(discardFrameErr < 0)
			{
				cout << "Error parsing discard path." << endl;
				return 1;
			}
			
			if(FileExists(discardFramePath))
			{
				cout << "Discard file already present " << discardFramePath << endl;
				return 1;
			}
		}
		
		
		if( IlmThread::supportsThreads() )
		{
			// get number of CPUs using Mach calls
			host_basic_info_data_t hostInfo;
			mach_msg_type_number_t infoCount;
			
			infoCount = HOST_BASIC_INFO_COUNT;
			host_info(mach_host_self(), HOST_BASIC_INFO,
					  (host_info_t)&hostInfo, &infoCount);
			
			setGlobalThreadCount(hostInfo.max_cpus);
		}
		
		
		int memWidth = 0;
		int memHeight = 0;
		
		map<string, void *> memBuffers;
		map<string, Imf::PixelType> memTypes;
		
		map<string, void *> halfMemBuffers;
		
		
		for(int f=startFrame; f <= endFrame; f++)
		{
			char inputFramePath[MAX_PATH_LEN];
			snprintf(inputFramePath, MAX_PATH_LEN, inputPath.c_str(), f);
			
			char tempFramePath[MAX_PATH_LEN];
			snprintf(tempFramePath, MAX_PATH_LEN, tempPath.c_str(), f);
			
			char discardFramePath[MAX_PATH_LEN];
			snprintf(discardFramePath, MAX_PATH_LEN, discardPath.c_str(), f);
			
			cout << string(inputFramePath).substr(lastSlash + 1) << endl;
			
			{
				MultiPartInputFile multiPartFile(inputFramePath);
				
				if(multiPartFile.parts() > 1)
				{
					cout << "Not handling multi-part files yet." << endl;
					continue;
				}
				
				if(multiPartFile.header(0).type() != SCANLINEIMAGE)
				{
					cout << "Only scanline images so far." << endl;
					continue;
				}
				
				InputPart inPart(multiPartFile, 0);
				
				const Header &head = inPart.header();
				const Box2i &dataW = head.dataWindow();
				
				const int dataWidth = (dataW.max.x - dataW.min.x) + 1;
				const int dataHeight = (dataW.max.y - dataW.min.y) + 1;
				
				if(memWidth != dataWidth || memHeight != dataHeight)
				{
					for(map<string, void *>::iterator i = memBuffers.begin(); i != memBuffers.end(); ++i)
					{
						if(i->second != NULL)
							free(i->second);
					}
					
					memBuffers.empty();
					memTypes.empty();
					
					for(map<string, void *>::iterator i = halfMemBuffers.begin(); i != halfMemBuffers.end(); ++i)
					{
						if(i->second != NULL)
							free(i->second);
					}
					
					halfMemBuffers.empty();
				}
				
				memWidth = dataWidth;
				memHeight = dataHeight;
				
				int numChannels = 0, numDiscardedChannels = 0, numFloatChannels = 0;
				FrameBuffer frameBuffer;
				
				ChannelList outputChannels;
				
				const ChannelList &channels = head.channels();
				
				for(ChannelList::ConstIterator i = channels.begin(); i != channels.end(); ++i)
				{
					if(convertToHalf ||
						i.name() == string("R") ||
						i.name() == string("G") ||
						i.name() == string("B") ||
						i.name() == string("A"))
					{
						if(i.channel().xSampling != 1 || i.channel().ySampling != 1)
						{
							cout << "Can't handle subsampling." << endl;
							return 1;
						}
						
						const size_t chanSize = (i.channel().type == Imf::FLOAT ? sizeof(float):
													i.channel().type == Imf::HALF ? sizeof(half):
													i.channel().type == Imf::UINT ? sizeof(uint32_t):
													sizeof(float));
						
						const size_t rowBytes = (chanSize * memWidth);
						const size_t bufSize = (rowBytes * memHeight);
						
						if(memBuffers.find(i.name()) == memBuffers.end() || memTypes[i.name()] != i.channel().type)
						{
							if(memBuffers.find(i.name()) != memBuffers.end() && memBuffers[i.name()] != NULL)
								free(memBuffers[i.name()]);
							
							memBuffers[i.name()] = malloc(bufSize);
							memTypes[i.name()] = i.channel().type;
						}
						
						char * const channel_origin = (char *)memBuffers[i.name()] - (chanSize * dataW.min.x) - (rowBytes * dataW.min.y);

						frameBuffer.insert(i.name(), Slice(i.channel().type,
															channel_origin,
															chanSize,
															rowBytes,
															i.channel().xSampling,
															i.channel().ySampling,
															0.0) );
															
						outputChannels.insert(i.name(), (convertToHalf && i.channel() == Imf::FLOAT ? Imf::HALF : i.channel()));
						
						numChannels++;
						
						if(i.channel().type == Imf::FLOAT)
							numFloatChannels++;
					}
					else
						numDiscardedChannels++;
				}
				
				if(numChannels == 0)
				{
					cout << "Can't leave a file with no channels" << endl;
					continue;
				}

				if(convertToHalf)
				{
					if(numFloatChannels == 0)
					{
						cout << "No float channels to convert" << endl;
						continue;
					}
				}
				else
				{
					if(numDiscardedChannels == 0)
					{
						cout << "No channels to discard" << endl;
						continue;
					}
				}
				
				
				inPart.setFrameBuffer(frameBuffer);
				
				inPart.readPixels(dataW.min.y, dataW.max.y);
				
				
				FrameBuffer halfFrameBuffer;
				
				if(convertToHalf)
				{
					TaskGroup taskGroup;
					
					for(FrameBuffer::ConstIterator i = frameBuffer.begin(); i != frameBuffer.end(); i++)
					{
						if(i.slice().type == Imf::FLOAT)
						{
							const size_t floatChanSize = sizeof(float);
							const size_t floatRowBytes = (floatChanSize * memWidth);
							
							const size_t halfChanSize = sizeof(half);
							const size_t halfRowBytes = (halfChanSize * memWidth);
							const size_t halfBufSize = (halfRowBytes * memHeight);
							
							if(halfMemBuffers.find(i.name()) == halfMemBuffers.end())
							{
								halfMemBuffers[i.name()] = malloc(halfBufSize);
							}
							
							for(int y=0; y < memHeight; y++)
							{
								const float *inputRow = (const float *)((char *)memBuffers[i.name()] + (y * floatRowBytes));
								half *outputRow = (half *)((char *)halfMemBuffers[i.name()] + (y * halfRowBytes));
								
								ThreadPool::addGlobalTask(new FloatToHalfTask(&taskGroup, inputRow, outputRow, memWidth));
							}
							
							char * const half_channel_origin = (char *)halfMemBuffers[i.name()] - (halfChanSize * dataW.min.x) - (halfRowBytes * dataW.min.y);

							halfFrameBuffer.insert(i.name(), Slice(Imf::HALF,
																	half_channel_origin,
																	halfChanSize,
																	halfRowBytes,
																	i.slice().xSampling,
																	i.slice().ySampling,
																	0.0));
						}
						else
							halfFrameBuffer.insert(i.name(), i.slice());
					}
				}
				
				
				Header outputHeader(head);
				
				outputHeader.channels() = outputChannels;
				
				OutputFile outputFile(tempFramePath, outputHeader);
				
				outputFile.setFrameBuffer(convertToHalf ? halfFrameBuffer : frameBuffer);
				
				outputFile.writePixels(dataHeight);
			}
			
			const int discardErr = rename(inputFramePath, discardFramePath);
			
			if(discardErr != 0)
			{
				cout << "Error moving original" << endl;
				cout << "Deleting stripped file" << endl;
				cout << tempFramePath << endl;
				
				const int deleteErr = remove(tempFramePath);
				
				if(deleteErr != 0)
				{
					cout << "Great, couldn't even delete the new file" << endl;
				}
				
				return 1;
			}
			
			const int replaceErr = rename(tempFramePath, inputFramePath);
			
			if(replaceErr != 0)
			{
				cout << "Error replacing original" << endl;
				cout << "Stripped file remains" << endl;
				cout << tempFramePath << endl;
				
				return 1;
			}
		}
		
		for(map<string, void *>::iterator i = memBuffers.begin(); i != memBuffers.end(); ++i)
		{
			if(i->second != NULL)
				free(i->second);
		}
		
		for(map<string, void *>::iterator i = halfMemBuffers.begin(); i != halfMemBuffers.end(); ++i)
		{
			if(i->second != NULL)
				free(i->second);
		}
	}
	catch(const exception &e)
	{
		cout << "Exception: " << e.what() << endl;
		return 1;
	}
	catch(...)
	{
		cout << "Unknown exception thrown." << endl;
		return 1;
	}
	
	return 0;
}
